import java.sql.Connection;

import Factory.ConversionType;
import Factory.Converter;
import Factory.ConverterFactory;

public class Parsing {

	public static void main(String[] args) {

		ConverterFactory cf = new ConverterFactory();

		// Uncomment any of the below comments to try the corresponding type of
		// conversion

		Converter c1 = cf.getConverter(ConversionType.XMLTOCSV);
		//c1.convert();

		Converter c2 = cf.getConverter(ConversionType.CSVTOJSON);
		//c2.convert();

		Converter c3 = cf.getConverter(ConversionType.EXCELTOCSV);
		//c3.convert();

		Converter c4 = cf.getConverter(ConversionType.JSONTOPOJO);
		//c4.convert();

	}

}
