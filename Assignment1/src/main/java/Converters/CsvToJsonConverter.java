package Converters;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import DatabaseFunctions.Database;
import Factory.ConversionType;
import Factory.Converter;

public class CsvToJsonConverter implements Converter {

	public void convert() {

		File input = new File("src/main/resources/books.csv");
		File output = new File("src/main/resources/CsvToJsonOutput.json");

		CsvSchema csvSchema = CsvSchema.builder().setUseHeader(true).build();
		CsvMapper csvMapper = new CsvMapper();

		// Reading CSV data from input file into a list of objects
		List<Object> readAll = null;
		try {
			readAll = csvMapper.readerFor(Map.class).with(csvSchema).readValues(input).readAll();
		} catch (JsonProcessingException e) {

		} catch (IOException e) {
			e.printStackTrace();
		}

		ObjectMapper mapper = new ObjectMapper();

		// Writing the data in JSON format into output file
		try {

			mapper.writerWithDefaultPrettyPrinter().writeValue(output, readAll);

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Creating table for JSON data in the database and concatenating the insert
		// queries
		// into a string builder

		Database.createTable(ConversionType.CSVTOJSON);
		StringBuilder insertQueries = new StringBuilder();

		JSONParser jsonParser = new JSONParser();

		try {
			FileReader reader = new FileReader("src/main/resources/CsvToJsonOutput.json");
			Object obj = jsonParser.parse(reader);
			JSONArray bookList = (JSONArray) obj;

			for (Object book : bookList) {
				insertQueries.append("insert into books values(");
				insertQueries.append(
						"'" + ((String) ((JSONObject) book).get("Title")).replace(",", "").replace("'", "") + "',");
				insertQueries.append("'" + ((String) ((JSONObject) book).get("Author")).replace(",", "") + "',");
				insertQueries.append("'" + ((JSONObject) book).get("Genre") + "',");
				insertQueries.append(((JSONObject) book).get("Height") + ",");
				insertQueries.append("'" + ((String) ((JSONObject) book).get("Publisher")).replace("'", "") + "'");
				insertQueries.append(");\n");
			}

			// Inserting Json data into the database
			Database.insertData(insertQueries.toString());

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
