package Converters;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import DatabaseFunctions.Database;
import Factory.ConversionType;
import Factory.Converter;

public class JsonToPojoConverter implements Converter {

	public void convert() {
		JSONParser jsonParser = new JSONParser();

		FileReader reader;
		try {
			// Read JSON file
			reader = new FileReader("src/main/resources/users.json");
			JSONObject obj = (JSONObject) jsonParser.parse(reader);

			// Access the array of users from the JSONObject and store it into a String
			String json = (obj.get("users")).toString();
			ObjectMapper mapper = new ObjectMapper();

			// Map the users from json String into a collection of type User
			List<User> users = mapper.readValue(json,
					mapper.getTypeFactory().constructCollectionType(List.class, User.class));

			// Create table in the database and instantiate a StringBuilder to concatenate
			// insert queries
			Database.createTable(ConversionType.JSONTOPOJO);
			StringBuilder insertQueries = new StringBuilder();

			for (User u : users) {
				insertQueries.append("insert into users values(");
				insertQueries.append(u.getUserId() + ",");
				insertQueries.append("'" + u.getFirstName() + "',");
				insertQueries.append("'" + u.getLastName() + "',");
				insertQueries.append(u.getPhoneNumber() + ",");
				insertQueries.append("'" + u.getEmailAddress() + "');\n");

			}
			Database.insertData(insertQueries.toString());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

}
