package Converters;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import DatabaseFunctions.Database;
import Factory.ConversionType;
import Factory.Converter;
import au.com.bytecode.opencsv.CSVWriter;

public class ExcelToCsvConverter implements Converter {

	public void convert() {

		Iterator<Row> rowIterator = null;
		FileInputStream input_document = null;
		CSVWriter my_csv_output = null;

		try {
			input_document = new FileInputStream(new File("src/main/resources/persons.xls"));
			HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document);
			HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
			// To iterate over the rows
			rowIterator = my_worksheet.iterator();
			// OpenCSV writer object to create CSV file
			FileWriter my_csv = new FileWriter("src/main/resources/XlsToCsvOutput.csv");
			my_csv_output = new CSVWriter(my_csv);

		} catch (IOException e) {

		}
		// Loop through rows.
		Database.createTable(ConversionType.EXCELTOCSV);
		StringBuilder insertQueries = new StringBuilder();
		int count = 0;

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			int i = 0;
			String[] csvdata = new String[8];
			if (count == 1)
				insertQueries.append("insert into person values(");
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				switch (cell.getCellType()) {

				case Cell.CELL_TYPE_STRING:
					csvdata[i] = cell.getStringCellValue();
					break;
				case Cell.CELL_TYPE_NUMERIC:
					csvdata[i] = String.valueOf(((int) cell.getNumericCellValue()));
					break;
				}
				if (count == 1)
					insertQueries.append("'" + csvdata[i] + "',");

				i = i + 1;

			}
			if (count == 1)
				insertQueries.deleteCharAt(insertQueries.length() - 1).append(");\n");
			count = 1;
			my_csv_output.writeNext(csvdata);
		}

		try {
			Database.insertData(insertQueries.toString());
			my_csv_output.close();
			input_document.close();
		} catch (IOException e) {

		}

	}

}
