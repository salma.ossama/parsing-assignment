package Converters;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import DatabaseFunctions.Database;
import Factory.ConversionType;
import Factory.Converter;

public class XmlToCsvConverter implements Converter {

	public void convert() {

		// DocumentBuilderFactory and DocumentBuilder were used to parse the XML file

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {

			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("src/main/resources/food.xml");
			NodeList foodList = doc.getElementsByTagName("food");

			// Data parsed will be stored into the StringBuilder sb then writen into the
			// output CSV file
			// using the PrintWriter

			PrintWriter writer = new PrintWriter(new File("src/main/resources/XmlToCsvOutput.csv"));
			StringBuilder sb = new StringBuilder();

			// We start by adding the column name into the CSV file by getting the tags of
			// the first
			// element in the XML file

			NodeList firstItem = foodList.item(0).getChildNodes();

			for (int k = 0; k < firstItem.getLength(); k++) {
				Node currentTag = firstItem.item(k);
				if (currentTag.getNodeType() == Node.ELEMENT_NODE) {
					Element currentTagElement = (Element) currentTag;

					if (k < firstItem.getLength() - 2)
						sb.append(currentTagElement.getTagName() + ",");
					else
						sb.append(currentTagElement.getTagName());

				}
			}
			sb.append("\n");

			Database.createTable(ConversionType.XMLTOCSV);
			StringBuilder insertQueries = new StringBuilder();

			// Now that we're done adding the column names, we will start filling our CSV
			// with the data
			// row by row using the following two nested loops to access the food list first
			// then the second one to Iterate over the tags of the current food

			for (int i = 0; i < foodList.getLength(); i++) {
				Node f = foodList.item(i);
				if (f.getNodeType() == Node.ELEMENT_NODE) {
					Element food = (Element) f;
					NodeList list = food.getChildNodes();

					insertQueries.append("insert into food values(");
					for (int j = 0; j < list.getLength(); j++) {
						Node n = list.item(j);
						if (n.getNodeType() == Node.ELEMENT_NODE) {
							Element info = (Element) n;
							if (j < list.getLength() - 2) {
								sb.append(info.getTextContent() + ",");
								insertQueries.append("'" + info.getTextContent() + "'" + ",");
							} else {
								sb.append(info.getTextContent());
								insertQueries.append(info.getTextContent() + ");\n");
							}
						}
					}
					sb.append("\n");
				}

			}

			// Now that we've added all the data in our string builder we will write the
			// data into the output file and close the PrintWriter

			Database.insertData(insertQueries.toString());
			writer.write(sb.toString());
			writer.close();

		} catch (ParserConfigurationException e) {

			e.printStackTrace();

		} catch (SAXException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}
}
