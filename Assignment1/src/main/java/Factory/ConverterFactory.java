package Factory;

import Converters.CsvToJsonConverter;
import Converters.ExcelToCsvConverter;
import Converters.JsonToPojoConverter;
import Converters.XmlToCsvConverter;

public class ConverterFactory {

	public Converter getConverter(ConversionType type) {

		switch (type) {

		case XMLTOCSV:
			return new XmlToCsvConverter();
		case JSONTOPOJO:
			return new JsonToPojoConverter();
		case EXCELTOCSV:
			return new ExcelToCsvConverter();
		case CSVTOJSON:
			return new CsvToJsonConverter();
		default:
			return null;

		}

	}

}
