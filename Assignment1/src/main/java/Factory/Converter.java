package Factory;

public interface Converter {

	public void convert();

}
