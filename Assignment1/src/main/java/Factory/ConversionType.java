package Factory;

public enum ConversionType {

	XMLTOCSV, JSONTOPOJO, EXCELTOCSV, CSVTOJSON;

}
