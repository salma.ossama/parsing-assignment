package DatabaseFunctions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import Factory.ConversionType;

public class Database {

	static Database db = new Database();

	public Connection connectToDatabase() {

		Connection connection = null;

		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5000/Parsing", "postgres",
					"Ss1234567");

		} catch (Exception e) {
			System.out.println("Couldn't connect to the database");

		}
		return connection;

	}

	public static void createTable(ConversionType type) {

		Connection c = db.connectToDatabase();
		Statement s = null;
		try {
			String createQuery;

			switch (type) {
			case XMLTOCSV:
				createQuery = "create table food(name varchar(50),price money,description text, calories int, Unique(name,price,description,calories));";
				break;
			case CSVTOJSON:
				createQuery = "create table books(title text,author varchar(30),genre varchar(30), height int,publisher varchar(30), Unique(title,author,genre,height,publisher));";
				break;
			case EXCELTOCSV:
				createQuery = "create table person(Number int, Firstname varchar(30), Lastname varchar(30), Gender varchar(20), Country varchar(20) , Age int , Date text, ID int,unique(Number,Firstname,Lastname,Gender,Country,Age,Date,ID));";
				break;
			case JSONTOPOJO:
				createQuery = "create table users(userID int, Firstname varchar(20),Lastname varchar(20),phonenumber int,email text, unique(userID, Firstname,Lastname,phonenumber,email));";
				break;
			default:
				createQuery = "";

			}

			s = c.createStatement();
			s.executeUpdate(createQuery);
		} catch (Exception e) {
			System.out.println("Tables already created!");
		}

	}

	public static void insertData(String insertQuery) {

		Connection c = db.connectToDatabase();
		Statement s = null;
		try {
			s = c.createStatement();
			s.executeUpdate(insertQuery);
		} catch (Exception e) {
			System.out.println("Can't insert duplicate values");
		}

	}

}
